﻿using UnityEngine;

public static class LibGameObject
{
    public static GameObject FindChildByName(GameObject parent, string childName)
    {
        foreach (Transform transform in parent.transform)
        {
            if (transform.name == childName)
            {
                return transform.gameObject;
            }
            else
            {
                GameObject recursiveFound = FindChildByName(transform.gameObject, childName);
                if (recursiveFound != null)
                {
                    return recursiveFound;
                }
            }
        }
        return null;
    }
}
