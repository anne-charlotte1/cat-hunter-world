﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPSCameraController : MonoBehaviour
{
  public float Speed = 1.0f;
  public Transform Target;
  public Transform Player;
  public GameObject PlayerObject;

  float x, y;

  void Start() {
    Cursor.visible = false;
    Cursor.lockState = CursorLockMode.Locked;
  }

  void Update() {
    x += Input.GetAxis("Mouse X") * Speed;
    y -= Input.GetAxis("Mouse Y") * Speed;
    transform.LookAt(Target);
    Target.rotation = Quaternion.Euler(y, x, 0.0f);
    Player.rotation = Quaternion.Euler(0.0f, x, 0.0f);
    var dir = (transform.position - Player.position);
    dir.Normalize();
    RaycastHit hit;
    var hasHit = Physics.Raycast(Target.position, dir, out hit, 4.5f);
    // foreach (var hit in hits) {
    if (hasHit) {
      Renderer rend = hit.transform.GetComponent<Renderer>();

            if (rend)
            {
                // Change the material of all hit colliders
                // to use a transparent shader.
                rend.material.shader = Shader.Find("Transparent/Diffuse");
                Color tempColor = rend.material.color;
                tempColor.a = 0.3F;
                rend.material.color = tempColor;
            }

      if (hit.collider == PlayerObject)
        Debug.Log("Player is hit");
      Debug.Log(hit.collider.gameObject.name);
      hit.collider.gameObject.name = "hit";
      transform.position = Target.position + dir * hit.distance;
      // Debug.Log(hit.collider.gameObject.transform);
    }
    Debug.Log("-------------");
  }
}
