﻿using UnityEngine;

public class SensitivityManager : MonoBehaviour
{
    void Start()
    {
        if (PlayerPrefs.HasKey("settingsSensitivity"))
        {
            int selectedSensitivity = PlayerPrefs.GetInt("settingsSensitivity");
            Cinemachine.CinemachineFreeLook cameraManager = GetComponent<Cinemachine.CinemachineFreeLook>();
            cameraManager.m_XAxis.m_MaxSpeed = selectedSensitivity * 100.0f;
            cameraManager.m_YAxis.m_MaxSpeed = selectedSensitivity;
        }
    }
}
