﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceKillCount : MonoBehaviour
{
    public Gamemode gamemode;

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
        text.text = gamemode.mouseKilled.ToString() + (gamemode.mouseKilled == 69 ? " MOUSES KILLED, NICE!!!" : gamemode.mouseKilled > 1 ? " MOUSES KILLED" : " MOUSE KILLED"); ;
    }
}
