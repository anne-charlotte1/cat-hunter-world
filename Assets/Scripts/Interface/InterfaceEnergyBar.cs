﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceEnergyBar : MonoBehaviour
{
    public Gamemode gamemode;

    private Slider slider;

    private void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = gamemode.maximumTime;
    }

    void Update()
    {
        slider.value = gamemode.currentTime;
    }
}
