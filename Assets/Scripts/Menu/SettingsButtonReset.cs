﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsButtonReset : MenuButton
{
    public Text buttonText;

    protected override void ButtonAction()
    {
        if (!exitScene)
        {
            buttonText.text = "Confirm Reset?";
            exitScene = true;
        }
        else
        {
            string scoreListStr = PlayerPrefs.HasKey("gameScoreList") ? PlayerPrefs.GetString("gameScoreList") : null;
            PlayerPrefs.DeleteAll();
            if (scoreListStr != null)
            {
                PlayerPrefs.SetString("gameScoreList", scoreListStr);
                PlayerPrefs.Save();
            }
            SceneManager.LoadScene("SettingsMenu", LoadSceneMode.Single);
        }
    }
}
