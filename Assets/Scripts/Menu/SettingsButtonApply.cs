﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonApply : MenuButton
{
    public Slider resolutionSlider;
    public Slider qualitySlider;
    public Slider soundSlider;
    public Slider sensitivitySlider;

    protected override void ButtonAction()
    {
        // Save settings
        PlayerPrefs.SetInt("settingsResolution", (int)resolutionSlider.value);
        PlayerPrefs.SetInt("settingsQuality", (int)qualitySlider.value);
        PlayerPrefs.SetInt("settingsSound", (int)soundSlider.value);
        PlayerPrefs.SetInt("settingsSensitivity", (int)sensitivitySlider.value);
        PlayerPrefs.Save();

        // Apply new settings
        Resolution selectedResolution = Screen.resolutions[PlayerPrefs.GetInt("settingsResolution")];
        Screen.SetResolution(selectedResolution.width, selectedResolution.height, true, selectedResolution.refreshRate);
        int selectedQuality = PlayerPrefs.GetInt("settingsQuality");
        QualitySettings.SetQualityLevel(selectedQuality);
        float selectedVolume = PlayerPrefs.GetInt("settingsSound") / 100.0f;
        music.volume = selectedVolume;
        click0.volume = selectedVolume;
        click1.volume = selectedVolume;
        click2.volume = selectedVolume;
    }
}
