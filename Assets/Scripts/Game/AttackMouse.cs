﻿using UnityEngine;

public class AttackMouse : MonoBehaviour
{
    public ParticleSystem particleBlood;
    public Gamemode gamemode;
    public AudioSource attackSound;
    public AudioSource killSound;

    public string mouseTag = "Target";
    public float attackDistance = 0.5f;
    public float attackTimeCost = 1.0f;

    void Update()
    {
        if ((int)gamemode.gameInProgress > 0 && Input.GetMouseButtonDown(0))
        {
            attackSound.Play();
            gamemode.currentTime -= attackTimeCost;
            GameObject[] mouseList = GameObject.FindGameObjectsWithTag(mouseTag);
            Vector3 playerPos = transform.position;
            foreach (GameObject mouse in mouseList)
            {
                Vector3 mousePos = mouse.transform.position;
                float mouseDistance = Vector3.Distance(playerPos, mousePos);
                if (mouseDistance < attackDistance)
                {
                    gamemode.killMouse(mouse);
                    killSound.Play();
                    particleBlood.Clear();
                    particleBlood.transform.position = mousePos;
                    particleBlood.Play();
                }
            }
        }
    }
}