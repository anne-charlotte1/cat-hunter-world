﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Score
{
    public int score;
    public string time;
}

public static class ScoreboardManager
{
    private static char limitChar = ';';
    private static char sublimitChar = ',';

    public static void registerScore(int score)
    {
        string scoreStr = score.ToString() + sublimitChar + DateTime.Now.ToString().Split(' ')[0];
        if (PlayerPrefs.HasKey("gameScoreList"))
        {
            string scoreListStr = PlayerPrefs.GetString("gameScoreList");
            scoreListStr += limitChar + scoreStr;
            PlayerPrefs.SetString("gameScoreList", scoreListStr);
        }
        else
        {
            PlayerPrefs.SetString("gameScoreList", scoreStr);
        }
        PlayerPrefs.Save();
    }

    public static List<Score> getScoreList()
    {
        List<Score> scoreList = new List<Score>();
        if (PlayerPrefs.HasKey("gameScoreList"))
        {
            string scoreListStr = PlayerPrefs.GetString("gameScoreList");
            string[] scoreListArray = scoreListStr.Split(limitChar);
            foreach (string scoreStr in scoreListArray)
            {
                string[] scoreArray = scoreStr.Split(sublimitChar);
                scoreList.Add(new Score
                {
                    score = int.Parse(scoreArray[0]),
                    time = scoreArray[1]
                });
            }
        }
        return scoreList;
    }
}
